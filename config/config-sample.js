/**
	Configuration File
	http://stackoverflow.com/questions/5869216/how-to-store-node-js-deployment-settings-configuration-files
**/

var config = {}

config.email = {};

config.email.password = '';
config.email.username = '';
config.email.port = 578;
config.email.host = '';

config.email.defaults = {}
config.email.defaults.to = ';
config.email.defaults.from = '';
config.email.defaults.envelope: {
	from: "",
	to: ""
}

module.exports = config;