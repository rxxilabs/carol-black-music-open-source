var express 		= require('express');
var app 			= express();

var config 			= require('./config/config.js')

var bodyParser = require('body-parser');
var jsonParser = bodyParser.json()
var urlencodedParser = bodyParser.urlencoded({ extended: false })

var nodemailer 		= require("nodemailer");

app.use(express.static(__dirname + '/public'));

app.post('/api/v1/form/booking', urlencodedParser, function( req, res ) {
	var d = new Date();
	
	var smtpTransport = nodemailer.createTransport("SMTP",{
		host: config.email.host, // hostname
		secureConnection: false, // use SSL
		port: config.email.port, // port for secure SMTP
		auth: {
			user: config.email.username,
			pass: config.email.password
		}
	});
	
	var mailOptions = {
		from: config.email.defaults.from,
		to: config.email.defaults.to,
		subject: "carolblackmusic.co.uk: Website Contact Message from" + req.body.name,
		text: "From: " + req.body.name + "\n\n Email: " + req.body.email + "\n\n Phone: " + req.body.tel + "\n\n Message: " + req.body.message,
		html: "<strong>From</strong>: " + req.body.name + "<br/><strong>Email</strong>: " + req.body.email + "<br/><strong>Phone</strong>: " + req.body.tel + "<br><strong>Message</strong>: " + req.body.message,
		envelope: config.email.defaults.envelope
	}
	
	smtpTransport.sendMail(mailOptions, function(error, response){
		if(error){
			res.end('{"error" : error, "status" : 401}');
		}

		smtpTransport.close();
	});
	
	res.end('{"success" : "Updated Successfully", "status" : 200}');
	
});

app.get('/', function(req, res) {
	res.sendfile('./public/index.html');
});

app.get('*', function(req, res) {
	res.redirect('/');
});

app.use(express.static(__dirname + '/public/libs'));

var server = app.listen(3001, function() {
	console.log('Listening on port %d', server.address().port);
});